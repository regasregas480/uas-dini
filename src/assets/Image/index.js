import Novel1 from './Angkasa.jpg'
import Novel2 from './Dear.jpg'
import Novel3 from './LautTengah.jpg'
import Popular1 from './172.jpg'
import Popular2 from './AZ.jpg'
import Popular3 from './Ayah.jpeg'
import LogoUser from './user.jpg'
import LogoDiskon from './diskon.png'

export {
    Novel1, Novel2, Novel3, Popular1, Popular2, Popular3, LogoUser, LogoDiskon
}