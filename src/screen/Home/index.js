import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { LogoUser, Novel1, Novel2, Novel3, Popular1, Popular2, Popular3, LogoDiskon } from '../../assets'

const Home = ({navigation}) => {
  return (
    <View style={styles.body}>
        <ScrollView>
            

            <View style={styles.baris}>
                <View>
                    <Text style={{fontWeight: 'bold', color: '#0049c1', fontSize: 20,}}>Selamat Datang, DINI EKAWATI!</Text>
                </View>
                <View>
                </View>
            </View>
            <View style={styles.card}>
                <View style={{marginTop: 15}}>
                    <Text style={{fontWeight: 'bold', color: 'black', fontSize: 15, marginTop: 5,}}>Diskon Besar-Besaran</Text>
                    <Text style={{fontWeight: 'bold', color: 'black', fontSize: 10,}}>Baca Buku Harga MIring</Text>
                    <View style= {styles.cardbaris}>
                        <Text style={{fontWeight: 'bold', color: 'white', fontSize: 10,}}>Gasken Guys</Text>
                    </View>
                </View>
                <View>
                    <Image source={LogoDiskon} style={{width: 100, height: 50,}}/>
                </View>
            </View>
            <View style={styles.baris}>
                    <Text style={{fontWeight: 'bold', color: 'black', fontSize: 15, textAlign: 'center'}}>Terbaik Minggu Ini Guys!</Text>
                <Text style={{fontWeight: 'bold', color: 'black', fontSize: 15, textAlign: 'center'}}>Lihat semua</Text>
            </View>
            <View>
                <View style={styles.buku}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
                            <View style={styles.bukuDetail}>
                                <Image source={Novel1} style={{backgroundColor: 'red', width: 100, height: 160, borderRadius: 5,}}/>
                                <Text style={{color: 'black', fontWeight: 'bold'}}>Angkasa dan 56 Hari</Text>
                                <Text style={{color: '#ff00ae'}}>Rp.120.000</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
                            <View style={styles.bukuDetail}>
                                <Image source={Novel2} style={{backgroundColor: 'red', width: 100, height: 160, borderRadius: 5,}}/>
                                <Text style={{color: 'black', fontWeight: 'bold'}}>Dear Hyun Nam</Text>
                                <Text style={{color: '#ff00ae'}}>Rp.110.000</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
                            <View style={styles.bukuDetail}>
                                <Image source={Novel3} style={{backgroundColor: 'red', width: 100, height: 160, borderRadius: 5,}}/>
                                <Text style={{color: 'black', fontWeight: 'bold'}}>Laut Tengah</Text>
                                <Text style={{color: '#ff00ae'}}>Rp.80.000</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
                            <View style={styles.bukuDetail}>
                                <Image source={Novel1} style={{backgroundColor: 'red', width: 100, height: 160, borderRadius: 5,}}/>
                                <Text style={{color: 'black', fontWeight: 'bold'}}>Angkasa dan 56 Hari</Text>
                                <Text style={{color: '#ff00ae'}}>Rp.120.000</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
                            <View style={styles.bukuDetail}>
                                <Image source={Novel2} style={{backgroundColor: 'red', width: 100, height: 160, borderRadius: 5,}}/>
                                <Text style={{color: 'black', fontWeight: 'bold'}}>Dear Hyun Nam</Text>
                                <Text style={{color: '#ff00ae'}}>Rp.110.000</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
                        <View style={styles.bukuDetail}>
                            <Image source={Novel3} style={{backgroundColor: 'red', width: 100, height: 160, borderRadius: 5,}}/>
                            <Text style={{color: 'black', fontWeight: 'bold'}}>Laut Tengah</Text>
                            <Text style={{color: '#ff00ae'}}>Rp.80.000</Text>
                        </View>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </View>
            <View>
                <Text style={{fontWeight: 'bold', color: 'black', fontSize: 15, marginLeft: 20,}}>Penulis Terkenal</Text>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    <TouchableOpacity style={{marginLeft: 10, marginTop: 10,}}>
                        <View style={{justifyContent: 'center', alignItems: 'center', marginRight: 1, width: 70,}}>
                            <Image source={LogoUser} style={{backgroundColor: 'red', width: 50, height: 50, borderRadius: 50}}/>
                            <Text>User 1</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginLeft: 10, marginTop: 10,}}>
                        <View style={{justifyContent: 'center', alignItems: 'center', marginRight: 1, width: 70,}}>
                            <Image source={LogoUser} style={{backgroundColor: 'red', width: 50, height: 50, borderRadius: 50}}/>
                            <Text>User 2</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginLeft: 10, marginTop: 10,}}>
                        <View style={{justifyContent: 'center', alignItems: 'center', marginRight: 1, width: 70,}}>
                            <Image source={LogoUser} style={{backgroundColor: 'red', width: 50, height: 50, borderRadius: 50}}/>
                            <Text>User 3</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginLeft: 10, marginTop: 10,}}>
                        <View style={{justifyContent: 'center', alignItems: 'center', marginRight: 1, width: 70,}}>
                            <Image source={LogoUser} style={{backgroundColor: 'red', width: 50, height: 50, borderRadius: 50}}/>
                            <Text>User 4</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginLeft: 10, marginTop: 10,}}>
                        <View style={{justifyContent: 'center', alignItems: 'center', marginRight: 1, width: 70,}}>
                            <Image source={LogoUser} style={{backgroundColor: 'red', width: 50, height: 50, borderRadius: 50}}/>
                            <Text>User 5</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginLeft: 10, marginTop: 10,}}>
                        <View style={{justifyContent: 'center', alignItems: 'center', marginRight: 1, width: 70,}}>
                            <Image source={LogoUser} style={{backgroundColor: 'red', width: 50, height: 50, borderRadius: 50}}/>
                            <Text>User 6</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginLeft: 10, marginTop: 10,}}>
                        <View style={{justifyContent: 'center', alignItems: 'center', marginRight: 1, width: 70,}}>
                            <Image source={LogoUser} style={{backgroundColor: 'red', width: 50, height: 50, borderRadius: 50}}/>
                            <Text>User 7</Text>
                        </View>
                    </TouchableOpacity>
                </ScrollView>
            </View>
            <View style={{backgroundColor: '#ff8400', width: '100%', padding: 20 , height: 300, marginTop: 20}}>
                <Text style={{color: 'black', fontWeight: 'bold'}}>Novel Terpopuler</Text>
                <View style={{display: 'flex', flexDirection: 'row', marginTop: 20, alignItems: 'center'}}>
                    <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
                        <View style={styles.bukuDetail}>
                            <Image source={Popular1} style={{backgroundColor: 'red', width: 100, height: 160, borderRadius: 5,}}/>
                            <Text style={{color: 'white', fontWeight: 'bold'}}>172 Days</Text>
                            <Text style={{color: 'white'}}>Rp.80.000</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
                        <View style={styles.bukuDetail}>
                            <Image source={Popular2} style={{backgroundColor: 'red', width: 100, height: 160, borderRadius: 5,}}/>
                            <Text style={{color: 'white', fontWeight: 'bold'}}>Love from A to Z</Text>
                            <Text style={{color: 'white'}}>Rp.110.000</Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate("Detail")}>
                        <View style={styles.bukuDetail}>
                            <Image source={Popular3} style={{backgroundColor: 'red', width: 100, height: 160, borderRadius: 5,}}/>
                            <Text style={{color: 'white', fontWeight: 'bold'}}>Ayah</Text>
                            <Text style={{color: 'white'}}>Rp.90.000</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    </View>
  )
}

export default Home

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'white',
        width: '100%',
        height: '100%',
    },  
    baris: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        padding: 20,
    },
    card: {
        backgroundColor: '#ff950b',
        borderRadius: 5,
        margin: 20,
        height: 90,
        padding: 5,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
        },
    cardbaris: {
        backgroundColor: 'blue',
        marginTop: 5,
        marginBottom: 23,
        width: 80,
        height: 25,
        padding: 5,
    },
    buku: {
        margin: 35,
    },
    bukuDetail: {
        alignItems: 'center',
        marginRight: 20,
    }

})