import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { Novel1 } from '../../assets'

const Detail = ({navigation}) => {
  return (
    <View style={{backgroundColor: 'white', width: '100%', height: '100%'}}>
      <View style={{display: 'flex', flexDirection: 'row',height: 40,alignItems: 'center',borderBottomWidth: 1 ,}}>
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
          <Text style={{fontWeight: 'bold', color: 'black', marginLeft: 10}}>Kembali</Text> 
        </TouchableOpacity>
      </View>
        <View style={{backgroundColor: 'grey', height: 240, width: '100%', padding: 40,display: 'flex', flexDirection: 'row'}}>
          <View>
            <Image source={Novel1} style={{backgroundColor: 'red',borderWidth: 3, width: 100, height: 160, borderRadius: 5,}}/>
          </View>
          <View style={{padding: 50, paddingLeft: 20}}>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 20,}}>Angkasa Dan 56 Hari</Text>
            <TouchableOpacity style={{backgroundColor: 'orange', justifyContent: 'center' , width: 100, height: 40, marginLeft: 40, marginTop: 20, borderRadius: 10,}}>
              <Text style={{textAlign: 'center', fontSize: 14, color: 'black'}}>Beli Novel</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{padding: 30}}>
          <View>
            <Text style={{fontSize: 16, color: 'black', fontWeight: 'bold'}}>Deskripsi</Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{color: 'black'}}>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi, dan tata letak. Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi, dan tata letak. Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi, dan tata letak. Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi, dan tata letak. Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi, dan tata letak.</Text>
          </View>
        </View>
    </View>
  )
}

export default Detail

const styles = StyleSheet.create({})